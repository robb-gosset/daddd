Picotcp build command:

make ARCH=stm32 CROSS_COMPILE=arm-none-eabi- TCP=1 UDP=1 IPV4=1 IPFRAG=0 NAT=0 ICMP4=0 MCAST=0 DEVLOOP=0 PING=0 DHCP_CLIENT=1 DHCP_SERVER=0 DNS_CLIENT=0 IPFILTER=0 CRC=0 HTTP_CLIENT=0 HTTP_SERVER=1 ZMQ=0 OLSR=0 SLAACV4=0 STRIP=1 DEBUG=0

Toolchain
=========

Following instructions from http://marcelojo.org/marcelojoeng/2012/08/stm32f0-discovery-gcc-eclipse-openocd-ubuntu-part-i.html#more-10

Crosstool-ng from git version
make and install
create toolchain arm-unknown-eabi
saved to ~/x-tools/arm-unknown-eabi

install openocd-0.9.0 from src with ./configure --enable-maintainer-mode --enable-ft2232_libftdi --enable-ft2232_ftd2xx

openocd -f interface/ftdi/olimex-arm-usb-tiny-h.cfg -f board/olimex_stm32_p107.cfg

newlib, cloned from .. configured with

../newlib-cygwin/configure --prefix=`pwd` --target=arm-unknown-eabi

picotcp make command

make ARCH=stm32 CROSS_COMPILE=arm-none-eabi- TCP=1 UDP=1 IPV4=1 IPFRAG=0 NAT=0 ICMP4=0 MCAST=0 DEVLOOP=0 PING=0 DHCP_CLIENT=0 DHCP_SERVER=0 DNS_CLIENT=0 IPFILTER=0 CRC=0 HTTP_CLIENT=0 HTTP_SERVER=0 ZMQ=0 OLSR=0 SLAACV4=0 STRIP=1 DEBUG=0 
