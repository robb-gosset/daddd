/* Standard includes needed in whole project */



#include <stdint.h>
#include <stm32f10x_conf.h>
#include <stm32f10x.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_usart.h>

#include "usart.h"
