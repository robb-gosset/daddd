#ifndef __USART_H__
#define __USART_H__

#define mainCOM_PORT_BAUD_RATE				( 9600 )

int outbyte(int c);
void usartInit ( void );

#endif
