	*** OLIMEX demo project for the STM32-P107 ***

1. Requirements
	- STM32-P107 demo board by Olimex
	- Compatible debugger, for example ARM-JTAG-EW
	- IAR EW v6.30.7 or later
	- RS232 port and a terminal program (Hyper Terminal, PuTTY, etc.)
	- Network cable (RJ45) and a 10/100Mbit compatible Router/Switch
	
2. Description
	The demo demonstrates the functionality of the Ethernet module on the board. You can access the embedded web server and the ICMP server.
	
3. How to use this demo
	+ Open the workspace file: .\STM32-P107_DemoSoft_revC\STM32-P107_revC.eww
	+ There are 4 demos in this workspace. Select the one you need.
	+ Press F7 to compile the project.
	+ Connect the debugger to the PC and to the target board. Supply the board as needed.
	+ Press Ctrl+D to download the executable to the target.
	+ Connect the RS232 cable to the board and start your terminal program with the following settings: 115200-8N1
	+ Start debugging (F5)
	+ Follow the instructions on the terminal.
	+ The IP of the board will be displayed on the terminal, enter the IP in you favourite browser to explore the embedded web server.
		
4. Support
	http://www.iar.com/
	http://www.olimex.com/dev/
	http://www.sics.se/~adam/uip/index.php/Main_Page