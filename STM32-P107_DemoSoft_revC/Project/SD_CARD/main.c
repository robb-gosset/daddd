/**
******************************************************************************
* @file    Project/Template/main.c
* @author  MCD Application Team
* @version V3.1.0
* @date    06/19/2009
* @brief   Main program body
******************************************************************************
* @copy
*
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
* TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
* DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
* FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
* CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*
* <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
*/

/* Includes ------------------------------------------------------------------*/
#include "includes.h"
#include <yfuns.h>

/** @addtogroup Template_Project
* @{
*/

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
Int32U CriticalSecCntr;
USART_InitTypeDef USART_InitStructure;

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

static int MyLowLevelGetchar(void);

void SysTickStart(uint32_t Tick)
{
	RCC_ClocksTypeDef Clocks;
	volatile uint32_t dummy;

	RCC_GetClocksFreq(&Clocks);

	dummy = SysTick->CTRL;
	SysTick->LOAD = (Clocks.HCLK_Frequency/8)/Tick;

	SysTick->CTRL = 1;
}

void SysTickStop(void)
{
	SysTick->CTRL = 0;
}


#define UPDATE_SHOW_DLY       ((Int32U)(0.5 * 2))

int SDCard_Test(void)
{
    int ret = 1;
    Int32U Dly = UPDATE_SHOW_DLY;
    DiskStatusCode_t StatusHold = (DiskStatusCode_t) - 1;
    Int32U Size;
    static Int8U buff[SD_DEF_BLK_SIZE];
    int Multiply = 0, Factor;

    printf("Press Esc for exit\n\r");
    /*SysTick to times per second*/
    SysTickStart(2);

 // PCONP_bit.PCGPIO = 1; // enable clock

    SdDiskInit();

    __enable_interrupt();

    while ('\x1B' != getchar())
    {
        if (SysTick->CTRL & (1 << 16))
        {
            // Update MMC/SD card status
            SdStatusUpdate();
            if (Dly-- == 0)
            {
                // LCD show
                Dly = UPDATE_SHOW_DLY;
                // Current state of MMC/SD show
                pDiskCtrlBlk_t pMMCDiskCtrlBlk = SdGetDiskCtrlBkl();
                if (StatusHold != pMMCDiskCtrlBlk->DiskStatus)
                {
                    StatusHold = pMMCDiskCtrlBlk->DiskStatus;
                    switch (pMMCDiskCtrlBlk->DiskStatus)
                    {
                        case DiskCommandPass:
                            // Calculate MMC/SD size [MB]
                            if (pMMCDiskCtrlBlk->BlockSize >= 1024)
                            {
                                Multiply = 1;
                                Factor = pMMCDiskCtrlBlk->BlockSize / 1024;
                            }
                            else
                            {
                                Multiply = 0;
                                Factor = 1024 / pMMCDiskCtrlBlk->BlockSize;
                            }
                            if (Multiply)
                                Size = (pMMCDiskCtrlBlk->BlockNumb * Factor) / 1024;
                            else
                                Size = (pMMCDiskCtrlBlk->BlockNumb / Factor) / 1024;
                            if (pMMCDiskCtrlBlk->DiskType == DiskMMC)
                            {
                                printf("MMC Card - %d MB\n\r", Size);
                            }
                            else
                            {
                                printf("SD Card - %d MB\n\r", Size);
                            }

                            if (pMMCDiskCtrlBlk->WriteProtect == TRUE)
                            {
                                printf("Card is Write Protected\n\r");
                            }

                            for (volatile int i = 0; 100000 > i; i++);
                            if (DiskCommandPass == SdDiskIO(buff, pMMCDiskCtrlBlk->BlockNumb - 1, 1, DiskRead))
                            {
                                #define SIGNATURE 0xAA55A55A
                                if (SIGNATURE != *((Int32U *) buff))
                                {
                                    printf ("The signature is not written yet! Writing now...\n\r");
                                    if (FALSE == pMMCDiskCtrlBlk->WriteProtect)
                                    {
                                        *((Int32U *) buff) = SIGNATURE;
                                        if (DiskCommandPass != SdDiskIO(buff, pMMCDiskCtrlBlk->BlockNumb - 1, 1, DiskWrite) ||
                                            DiskCommandPass != SdDiskIO(buff, pMMCDiskCtrlBlk->BlockNumb - 1, 1, DiskVerify))
                                        {
                                            printf("The signature writing or verifying failed!\n\r");
                                            ret = 1;
                                        }
                                        else
                                        {
                                            printf ("The signature written successful!\n\r");
                                            ret = 0;
                                        }
                                    }
                                }
                                else
                                {
                                    printf ("The signature is already written! Reading successful!\n\r");
                                    ret = 0;
                                }
                            }
                            else
                            {
                                printf("Card I/O error\n\r");
                                ret = 1;
                            }
                            break;
                       case DiskNotReady:
                         printf ("Card not ready\n\r");
                         break;
                       case DiskUknowError:
                         printf ("Card unknown error\n\r");
                         break;
                       default:
                         printf("Pls, Insert Card\n\r");
                    }
                }
            }
        }
    }

    __disable_interrupt();

    return ret;
}

/**
* @brief  Main program.
* @param  None
* @retval None
*/
int main(void)
{
	/* Setup STM32 system (clock, PLL and Flash configuration) */
	SystemInit();

	/* USARTx configured as follow:
	- BaudRate = 115200 baud
	- Word Length = 8 Bits
	- One Stop Bit
	- No parity
	- Hardware flow control disabled (RTS and CTS signals)
	- Receive and transmit enabled
	*/
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	STM_EVAL_COMInit(COM1, &USART_InitStructure);

	/* Retarget the C library printf function to the USARTx, can be USART1 or USART2
	depending on the EVAL board you are using ********************************/
	printf("\fOLIMEX STM32P107 - SD card demo\n\r");

	/* Add your application code here
	*/

	SDCard_Test();
        
	printf("SD card demo is finished! Press button to send echo!\r\n");

	SysTickStop();

	STM_EVAL_GPIOReset();

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_OTG_FS | RCC_AHBPeriph_ETH_MAC |
	RCC_AHBPeriph_ETH_MAC_Tx | RCC_AHBPeriph_ETH_MAC_Rx , DISABLE);
	RCC_APB2PeriphClockCmd(~0xFFFF0002,DISABLE);
	//RCC_APB1PeriphClockCmd(~(0xC10137C0 | RCC_APB1Periph_USART3),DISABLE);

	/* Infinite loop */
	while (1)
	{
		int ch;
//		if(0 < ( ch = getchar()))
                ch = MyLowLevelGetchar();
                if(0 < ch)
		{
			putchar(ch);
		}
	}
}

/**
* @brief  Retargets the C library printf function to the USART.
* @param  None
* @retval None
*/

#ifdef  USE_FULL_ASSERT

/**
* @brief  Reports the name of the source file and the source line number
*   where the assert_param error has occurred.
* @param  file: pointer to the source file name
* @param  line: assert_param error line source number
* @retval None
*/
void assert_failed(uint8_t* file, uint32_t line)
{
	/* User can add his own implementation to report the file name and line number,
	ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1)
	{
	}
}
#endif

/**
* @}
*/

/*************************************************************************
* Function Name: __write
* Parameters: Low Level cahracter output
*
* Return:
*
* Description:
*
*************************************************************************/
size_t __write(int Handle, const unsigned char * Buf, size_t Bufsize)
{
	size_t nChars = 0;

	for (/*Empty */; Bufsize > 0; --Bufsize)
	{
		/* Loop until the end of transmission */
		while (USART_GetFlagStatus(EVAL_COM1, USART_FLAG_TXE) == RESET);
		USART_SendData(EVAL_COM1, * Buf++);
		++nChars;
	}
	return nChars;
}
/*************************************************************************
* Function Name: __read
* Parameters: Low Level cahracter input
*
* Return:
*
* Description:
*
*************************************************************************/
size_t __read(int handle, unsigned char * buffer, size_t size)
{
	int nChars = 0;

	/* This template only reads from "standard in", for all other file
* handles it returns failure. */
	if (handle != _LLIO_STDIN)
	{
		return _LLIO_ERROR;
	}

	for (/* Empty */; size > 0; --size)
	{
		int c = MyLowLevelGetchar();
		if (c < 0)
		break;

		*buffer++ = c;
		++nChars;
	}

	return nChars;
}

static int MyLowLevelGetchar(void)
{
	int ch;
	unsigned int status = EVAL_COM1->SR;

	if(status & USART_FLAG_RXNE)
	{
		ch = USART_ReceiveData(EVAL_COM1);
		if(status & (USART_FLAG_ORE | USART_FLAG_PE | USART_FLAG_FE) )
		{
			return (ch | 0x10000000);
		}
		return (ch & 0xff );
	}
	return -1;
}

/******************* (C) COPYRIGHT 2009 STMicroelectronics ***
	**END OF FILE****/
